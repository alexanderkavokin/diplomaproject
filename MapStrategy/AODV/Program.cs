﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AODV
{
    class Program
    {

        private static readonly List<Thread> Threads = new List<Thread>();
        static void Main(string[] args)
        {
//            RunStat();
            RunAgents();
        }

        private static void RunAgents()
        {
            var agents = Envinronment.Agents;
            Start(agents);
        }

        private static void RunStat()
        {
            var stat = new Statistic();
            stat.RunStatistic();
        }

        public static void Start(Dictionary<Agent, Coordinates> agents)
        {
            foreach (var key in agents.Keys)
            {
                var thread = new Thread(key.Life);
                Threads.Add(thread);
                thread.Start();
            }
            Console.ReadLine();
//            Stop();
        }

        static void Stop()
        {
            foreach (var thread in Threads)
            {
                thread.Abort();
            }
        }
    }
}
