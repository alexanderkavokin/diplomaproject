﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODV
{
    public static class Envinronment
    {
        public static Dictionary<Agent, Coordinates> Agents;
        private const int MapSize = 50;
        private const int RadioRadius = 7;
        private static char[,] _map = new char[MapSize,MapSize];

        static Envinronment()
        {
            Agents = new Dictionary<Agent, Coordinates>();
            RewriteFiles();
            Initialize();
        }

        static void RewriteFiles()
        {
            using (new StreamWriter("agents.txt"))
            { }
            using (new StreamWriter("log.txt"))
            { }
            using (new StreamWriter("map.txt"))
            { }
            using (new StreamWriter("table.txt")) 
            { }

            for (int i = 0; i < MapSize; i++)
            {
                for (int j = 0; j < MapSize; j++)
                {
                    _map[i, j] = '*';
                }
            }
        }

        static void Initialize()
        {
            var agentsList = AgentManager.CreateAgents();
            var rnd = new Random();

            Agents.Add(new Agent{Id = 0}, new Coordinates{X=0, Y=0});
            _map[0, 0] = '0';

            foreach (var agent in agentsList)
            {
                int x = rnd.Next(MapSize);
                int y = rnd.Next(MapSize);
                Agents.Add(agent, new Coordinates
                {
                    X = x,
                    Y = y
                });
//                char.TryParse(agent.Id.ToString(), out _map[y,x]);
            }
            SearchAgentsNeighbours();
            PrintAgentData();
//            PrintMap();
        }

        static void SearchAgentsNeighbours()
        {
            foreach (var agent in Agents)
            {
                foreach (var neighbour in Agents)
                {
                    var distance = GetDistance(agent.Value, neighbour.Value);
                    if (distance < RadioRadius && agent.Key.Id != neighbour.Key.Id)
                    {
                        agent.Key.Event += neighbour.Key.GetPREQ;
                        agent.Key.Neighbours.Add(neighbour.Key);
                    }
                    
                }
            }
        }

        static double GetDistance(Coordinates coord1, Coordinates coord2)
        {
            return Math.Sqrt(Math.Pow(coord1.X - coord2.X, 2) + Math.Pow(coord1.Y - coord2.Y, 2));
        }

        static void PrintAgentData()
        {
            using (var writer = new StreamWriter("agents.txt"))
            {
                foreach (var agent in Agents)
                {
                    writer.WriteLine("Agent #" + agent.Key.Id);
                    writer.WriteLine("Neighbours");
                    foreach (var neighbour in agent.Key.Neighbours)
                    {
                        writer.WriteLine(neighbour.Id);
                    }
                    writer.WriteLine();
                }
            }
        }

        static void PrintMap()
        {
            using (var writer = new StreamWriter("map.txt"))
            {
                for (int i = 0; i < MapSize; i++)
                {
                    for (int j = 0; j < MapSize; j++)
                    {
                        writer.Write(_map[i, j]);
                    }
                    writer.WriteLine();
                }
            }
        }
        private static readonly object _lockObj = new object();
        public static void MinusBattery(List<int> agents, int value)
        {
            lock (_lockObj)
            {
                foreach (var key in Agents.Keys)
                {
                    foreach (var agent in agents)
                    {
                        if (key.Id == agent)
                        {
                            key.BatteryEnergy -= value;
                            key.WriteCurrentBattery();
                        }
                    }
                }
            }
        }
    }

    public class Coordinates
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
