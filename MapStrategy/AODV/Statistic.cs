﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace AODV
{
    public class Statistic
    {
        Dictionary<Agent, Coordinates> agents = new Dictionary<Agent, Coordinates>();
        public void RunStatistic()
        {
            var timer = new Timer(10000);
            timer.Elapsed += SwitchOffAgents;
            timer.Enabled = true;
            timer.Start();
            agents = Envinronment.Agents;
            Program.Start(agents);
        }

        private void SwitchOffAgents(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            foreach (var agent in agents)
            {
                agent.Key.Switch = false;
            }
            var sortedAgents = agents.Keys.OrderByDescending(a => a.BatteryEnergy);
            using (var writer = new StreamWriter("statistic.txt", true))
            {
                writer.WriteLine("New iteration");
                foreach (var sortedAgent in sortedAgents)
                {
                    writer.WriteLine("agent #" + sortedAgent.Id + "   " + sortedAgent.BatteryEnergy);
                }
                writer.WriteLine();
                writer.WriteLine();
            }
            Console.WriteLine("Finish");
            agents = new Dictionary<Agent, Coordinates>();
            agents = Envinronment.Agents;
            Program.Start(agents);
        }
    }
}
