﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODV
{
    public class PREQ
    {
        public int Id { get; set; }

        public int SenderId { get; set; }
        public int RecepientId { get; set; }
        public List<int> Trace { get; set; }

        public int BatteryCost
        {
            get { return (18 + 9*Trace.Count) / 8; }
        }

        public PREQ()
        {
            Trace = new List<int>();
        }
    }
}
