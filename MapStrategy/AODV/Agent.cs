﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Timer = System.Timers.Timer;

namespace AODV
{
    public delegate void GetMessage(PREQ preq);
    public delegate void GetAnswer(PREQ preq);
    public class Agent
    {
        public int Id { get; set; }
        public List<Agent> Neighbours { get; set; }
        public List<int> TraceToBase { get; set; } 

        private const int EventsPropability = 100;
        public bool Switch = true;

        private Timer _timer = new Timer(1000);
        public int BatteryEnergy { get; set; }

        public object LockObj = new object();
        public object WriterLock = new object();
        public Dictionary<int, List<int>> Table;
        public bool IsTraceAvaivable = false;
        public Agent()
        {
            Neighbours = new List<Agent>();
            Table = new Dictionary<int, List<int>>();
            TraceToBase = new List<int>();
            BatteryEnergy = 100000;
            SetTimer();
        }

        private void SetTimer()
        {
            _timer.Enabled = true;
            _timer.Elapsed +=TimerOnElapsed;
            _timer.Start();
        }

        private void TimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            lock (WriterLock)
            {
                using (var writer = new StreamWriter("agent" + Id + ".txt", true))
                {
                    writer.WriteLine("Timer");
                    writer.WriteLine(BatteryEnergy);
                }
            }
        }

        public void Life()
        {
            if (!IsTraceAvaivable)
            {
                SendPREQ();
                IsTraceAvaivable = true;
            }
            // прокладывание маршрутизации
//            while (true)
//            {
//                
//            }

//            var rnd = new Random();
//            SendPREQ();
//            while (true)
//            {
//                if (rnd.Next(EventsPropability) == 99 && Id != 0)
//                {
//                    SendPREQ();
//                    break;
//                }
//            }
        }

        public void SendPREQ()
        {
            var preq = new PREQ();
            preq.SenderId = Id;
            preq.RecepientId = 0;
            preq.Trace.Add(Id);
            if (Event != null)
            {
                BatteryEnergy-=preq.BatteryCost;
                Event(preq);
            }
        }

        public event GetMessage Event;

        public event GetAnswer GetAnswerEvent;

        private void WriteLog(List<int> trace)
        {
            using (var writer = new StreamWriter("log.txt", true))
            {
                if (trace.Count > 0)
                {
                    writer.WriteLine("Инициатор: ");
                }
                foreach (var i in trace)
                {
                    writer.WriteLine(i);
                }
                writer.WriteLine();
            }
        }

        public void GetPREQ(PREQ preq)
        {
            if (preq.Trace.Contains(Id)) return;
            if (Id == 0)
            {
                lock (LockObj)
                {
                    WriteLog(preq.Trace);
                    UpdateTable(preq);

//                    Answer(preq);
                }
            }
            else
            {
                preq.Trace.Add(Id);
                Thread.Sleep(100);
                if (Event != null)
                {
                    BatteryEnergy-=preq.BatteryCost;
                    Event(preq);
                }
            }
        }


        public void GetAnswer(PREQ preq, int i)
        {
            if (preq.SenderId == Id)
            {
                if(TraceToBase.Count == 0 || TraceToBase.Count > preq.Trace.Count)
                    TraceToBase = preq.Trace;
                if (!_isStartedData)
                {
                    _isStartedData = true;
                    StartGenerateData();
                }
            }
            else
            {
                var neighbour = Neighbours.FirstOrDefault(n => n.Id == preq.Trace[i]);
                if (neighbour != null)
                {
                    neighbour.GetAnswer(preq, i - 1);
                }
            }
        }
        Random _rnd;
        private bool _isStartedData = false;
        private void StartGenerateData()
        {
            using (var dataTimer = new Timer())
            {
                _rnd = new Random();
                dataTimer.Enabled = true;
                dataTimer.Interval = 1000;
                dataTimer.Elapsed += CreateData;
                dataTimer.Start();
            }
        }

        private void CreateData(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            if (_rnd.Next(3) == 0)
            {
                var dataMessage = new DataMessage();
                dataMessage.Trace = TraceToBase;
                dataMessage.SenderId = Id;
                Envinronment.MinusBattery(TraceToBase, dataMessage.Weight);
            }
        }

        private void Answer(PREQ preq)
        {
            UpdateTable(preq);

            int num = preq.Trace.Count - 1;
            var neighbour = Neighbours.FirstOrDefault(n => n.Id == preq.Trace[num]);
            if (neighbour != null)
            {
                neighbour.GetAnswer(preq, num-1);
            }
        }
        public void SendAnswer(AnswerMessage msg, int position)
        {
            if (Id == msg.RecepientId)
            {
                lock (WriterLock)
                {
                    using (var writer = new StreamWriter("agent" + Id + ".txt", true))
                    {
                        foreach (var i in msg.Trace)
                        {
                            writer.Write(i + "->");
                        }
                        writer.Write('0');
                    }
                }
                
            }
            else
            {
                var nId = msg.Trace[position--];
                var neighbour = Neighbours.FirstOrDefault(n => n.Id == nId);
                if (neighbour != null)
                {
                    BatteryEnergy-= (18 + 9*msg.Trace.Count)/8;
                    neighbour.SendAnswer(msg, position);
                }
            }
        }

        private void UpdateTable(PREQ preq)
        {
            if (Table.ContainsKey(preq.SenderId))
            {
                if (Table[preq.SenderId].Count > preq.Trace.Count)
                {
                    //todo вынести в отдельный метод
                    var newTrace = preq.Trace.ToList();
                    Table[preq.SenderId] = newTrace;
//                    WriteTable();

                    SendAnswer(new AnswerMessage
                    {
                        RecepientId = preq.SenderId, 
                        Trace = newTrace
                    }, 
                    newTrace.Count-1);
                }
            }
            else
            {
                var newTrace = preq.Trace.ToList();
                Table.Add(preq.SenderId, newTrace);
//                WriteTable();
                SendAnswer(new AnswerMessage
                {
                    RecepientId = preq.SenderId, 
                    Trace = newTrace
                }, 
                newTrace.Count - 1);
            }
        }

        private void WriteTable()
        {
            using (var writer = new StreamWriter("table.txt", true))
            {
                writer.WriteLine("New version");
                foreach (var row in Table)
                {
                    writer.WriteLine(row.Key);
                    foreach (var i in row.Value)
                    {
                        writer.Write(i + "->");
                    }
                    writer.WriteLine('0');
                    writer.WriteLine();
                }
                writer.WriteLine();
                writer.WriteLine();
            }
        }

        public void WriteCurrentBattery()
        {
            using (var writer = new StreamWriter("agent"+Id+".txt", true))
            {
                writer.WriteLine("Пришло письмо");
                writer.WriteLine(BatteryEnergy);
            }
        }
    }
}
