﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODV
{
    public static class AgentManager
    {
        private const int AgentsCount = 20;
        public static List<Agent> CreateAgents()
        {
            var agents = new List<Agent>();

            for (int i = 1; i <= AgentsCount; i++)
            {
                agents.Add(new Agent
                {
                    Id = i
                });
            }
            return agents;
        }
    }
}
