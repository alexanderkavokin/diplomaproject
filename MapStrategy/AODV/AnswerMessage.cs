﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODV
{
    public class AnswerMessage
    {
        public int RecepientId { get; set; }
        public List<int> Trace { get; set; }

        public AnswerMessage()
        {
            Trace = new List<int>();
        }
    }
}
