﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODV
{
    public class DataMessage
    {
        public int Id { get; set; }
        public byte[] Data { get; set; }
        public int SenderId { get; set; }
        public List<int> Trace { get; set; }
        public int Weight { get { return (Trace.Count*9 + 9)/8 + 100; }}
        public DataMessage()
        {
            Data = new byte[10];
            Trace = new List<int>();
        }
    }
}
