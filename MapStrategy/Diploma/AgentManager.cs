﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Diploma
{
    public class AgentManager
    {
        #region Properties

        private const int AgentsCount = 3;
        public MapManager MapManager { get; set; }
        public Dictionary<Agent, MapCell> Agents { get; set; } 
        #endregion

        #region Constructors

        public AgentManager()
        {
            Agents = new Dictionary<Agent, MapCell>();
        }
        #endregion

        #region Members
        public void CreateAgents()
        {
            var rnd = new Random();
            for (int i = 0; i < AgentsCount; i++)
            {
                var agent = new Agent();
                int x = rnd.Next(MapManager.MapSize);
                int y = rnd.Next(MapManager.MapSize);
                var cell = MapManager.MapCells.First(mapCell => mapCell.X == x && mapCell.Y == y);
                cell.IsAgentHere = true;
                agent.Id = i;
                agent.Logger = new Logger("agent " + i + ".txt");
                agent.AgentsCount = AgentsCount;
                Agents.Add(agent, cell);
            }
//            StartGame();
        }

        private void StartGame()
        {
            foreach (var agent in Agents.Keys)
            {
                var thread = new Thread(agent.Life);
                thread.Start();
            }
        }

        public void StartWork()
        {
            while (true)
            {
                foreach (var agent in Agents)
                {
                    agent.Key.SendHelloMessage();
                }
            }
        }

        #endregion
    }
}
