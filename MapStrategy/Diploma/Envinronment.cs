﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diploma
{
    public static class Envinronment
    {
        public static MapManager MapManager { get; set; }
        public static AgentManager AgentManager { get; set; }

        public static Dictionary<Agent, MapCell> Agents{get; set; }

        public static int[,] DistanceTable;

        public static void Initialize(MapManager mapManager, AgentManager agentManager)
        {
            MapManager = mapManager;
            AgentManager = agentManager;
            Agents = agentManager.Agents;
            CalculateDistanceTable();
        }

        private static void CalculateDistanceTable()
        {
            DistanceTable = new int[Agents.Count, Agents.Count];
            foreach (var agent in Agents)
            {
                foreach (var agent2 in Agents)
                {
                    DistanceTable[agent.Key.Id, agent2.Key.Id] = CalculateDistance(agent.Value, agent2.Value);
                }
            }
        }

        private static int CalculateDistance(MapCell cell1, MapCell cell2)
        {
            return (int) Math.Pow(Math.Pow(cell1.X - cell2.X, 2) + Math.Pow(cell1.Y - cell2.Y, 2), 0.5);
        }

        public static void SendHelloMessage(HelloMessage helloMessage)
        {
            for (int i = 0; i < Agents.Count; i++)
            {
                if (DistanceTable[helloMessage.SenderId, i] <= helloMessage.RadioRadius && DistanceTable[helloMessage.SenderId, i] != 0)
                {
                    Agents.Keys.ElementAt(i).GetMessage(helloMessage);
                }
            }
        }
    }
}
