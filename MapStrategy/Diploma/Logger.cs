﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Diploma
{
    public class Logger
    {
        private readonly string _fileName;
        public Logger(string fileName)
        {
            _fileName = fileName;
            DeleteOldFile();
        }

        private void DeleteOldFile()
        {
            File.Delete(_fileName);
        }

        public void Write(string message)
        {
            using (var writer = new StreamWriter(_fileName, true))
            {
                writer.WriteLine(message);
            }
        }
    }
}
