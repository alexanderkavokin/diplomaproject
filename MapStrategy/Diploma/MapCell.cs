﻿namespace Diploma
{
    public class MapCell
    {
        public int X { get; set; }
        public int Y { get; set; }
        public bool IsAgentHere { get; set; }

        public char Symbol
        {
            get { return IsAgentHere ? '*' : '.'; }
        }
    }
}
