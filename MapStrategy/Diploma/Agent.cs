﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diploma
{
    public delegate void MessageDel();
    public class Agent
    {
        #region Properties
        public int Id { get; set; }
        public int RadioRadius { get; set; }
        public Logger Logger { get; set; }

        public bool IsAllListened { get; set; }
        public int AgentsCount { get; set; }

        private readonly Dictionary<int, int> _listenedAgents = new Dictionary<int, int>(); 
        private Dictionary<int, int> _knownAgents = new Dictionary<int, int>(); 
        #endregion

        #region Constructors

        public Agent()
        {
        }
        #endregion

        #region Members

        public void Life()
        {
//            Logger.Write("Начинаю работу");
        }

        public void CreateNet()
        {
            while (true)
            {
                
            }
        }

        #endregion

        public void SendHelloMessage()
        {
            Logger.Write("Радиус связи " + RadioRadius);
            var helloMsg = new HelloMessage();
            helloMsg.SenderId = Id;
            helloMsg.ListenedAgents = _listenedAgents;
//            helloMsg.ListenedAgents.
            helloMsg.RadioRadius = RadioRadius;
            Envinronment.SendHelloMessage(helloMsg);
            if (IsAllListened) return;
            RadioRadius++;
        }

        public void GetMessage(HelloMessage helloMessage)
        {
            if(_listenedAgents.Keys.Contains(helloMessage.SenderId)) return;
            _listenedAgents.Add(helloMessage.SenderId, helloMessage.RadioRadius);
            Logger.Write("Слышу агента "+ helloMessage.SenderId);
            if (_listenedAgents.Count == AgentsCount-1)
            {
                IsAllListened = true;
            }
            foreach (var agent in helloMessage.ListenedAgents)
            {
                if(_knownAgents.ContainsKey(agent.Key)) continue;
                _knownAgents.Add(agent.Key, agent.Value);
            }
        }
    }
}
