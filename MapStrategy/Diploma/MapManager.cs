﻿using System;
using System.Collections.Generic;
namespace Diploma
{
    public class MapManager
    {
        #region Properties

        public const int MapSize = 10;
        public List<MapCell> MapCells { get; set; } 
        #endregion
        #region Constructors

        public MapManager()
        {
            MapCells = new List<MapCell>();
        }
        #endregion

        #region Members

        public void CreateMap()
        {
            for (int x = 0; x < MapSize; x++)
            {
                for (int y = 0; y < MapSize; y++)
                {
                    MapCells.Add(new MapCell{X = x, Y = y});
                }
            }
        }

        public void PrintMap()
        {
            int count = 0;
            foreach (var mapCell in MapCells)
            {
                if (count++ == MapSize-1)
                {
                    Console.WriteLine(mapCell.Symbol);
                    count = 0;
                }
                else Console.Write(mapCell.Symbol);
            }
            Console.ReadLine();
        }
        #endregion
    }
}
