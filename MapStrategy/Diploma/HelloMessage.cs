﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diploma
{
    public class HelloMessage
    {
        public int SenderId { get; set; }
        /// <summary>
        /// key - id услышанного агента
        /// value - его радиус связи, при котором его слышно
        /// </summary>
        public Dictionary<int, int> ListenedAgents { get; set; }
        public int RadioRadius { get; set; }
    }
}
