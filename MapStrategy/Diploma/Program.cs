﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diploma
{
    class Program
    {
        static void Main(string[] args)
        {
            var mapManager = new MapManager();
            mapManager.CreateMap();
            var agentManager = new AgentManager {MapManager = mapManager};
            agentManager.CreateAgents();

            Envinronment.Initialize(mapManager, agentManager);
            agentManager.StartWork();
//            mapManager.PrintMap();
        }
    }
}
