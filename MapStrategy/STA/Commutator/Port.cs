﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using STA.Network;

namespace STA.Commutator
{
    public class Port
    {
        public int Id { get; set; }
        public int Price { get; set; }
        public Commutator Owner { get; set; }
        public Commutator Neighbour { get; set; }
        public int NeighbourId { get; set; }
        public Port NeighbourPort { get; set; }
        public int NeighbourPortId { get; set; }
        public int PriceToRoot = 0;
        public bool IsRoot { get; set; }
        public bool IsDestinate { get; set; }
        public int PriceToDestinate = 0;
    }
}
