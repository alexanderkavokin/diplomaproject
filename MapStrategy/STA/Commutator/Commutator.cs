﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using STA.Packages;

namespace STA.Commutator
{
    public class Commutator
    {
        public int Id { get; set; }
        public int Priority { get; set; } //dublicate ID?
        private int rootCommutator;
        private bool IsRoot;
        public List<Port> Ports { get; set; }
        public ElectionPackage ElectionPackage;
        private const int SleepTimeForElection = 200;


        public Commutator()
        {
            Ports = new List<Port>();
        }

        public void Life()
        {
            Election();
            
            GenerateBdpu();
            Print();
            Thread.Sleep(5000);
            OrderDestinitionPort();
        }

        private void OrderDestinitionPort()
        {
            foreach (var port in Ports)
            {
                if (port.PriceToDestinate < port.NeighbourPort.PriceToDestinate && port.NeighbourId != rootCommutator)
                {
                    port.IsDestinate = true;
                    port.NeighbourPort.IsDestinate = false;
                }
            }
        }

        public void GetBdpu(BDPU bdpu, Port port)
        {
            bdpu.Cost += port.Price;
            port.PriceToRoot = bdpu.Cost;
            port.PriceToDestinate += port.Price;
            var list = Ports.OrderBy(p => p.PriceToRoot).ToList();
            foreach (var port1 in Ports)
            {
                port1.IsRoot = false;
            }
            list[0].IsRoot = true;
//            port.IsRoot = (bdpu.Cost <= port.PriceToRoot);
            SendBdpu(bdpu, port);
        }

        private void SendBdpu(BDPU bdpu, Port currentPort = null)
        {
            bdpu.WayList.Add(this);
            foreach (var port in Ports)
            {
                if (currentPort == port) continue;
                if (port.NeighbourId == bdpu.RootCommutatorId) continue;
//                if (port.IsRoot) continue;
                port.Neighbour.GetBdpu(bdpu, port.NeighbourPort);
            }
        }

        private void GenerateBdpu()
        {
            if (!IsRoot) return;
            var bdpu = new BDPU();
            bdpu.RootCommutatorId = Id;
            bdpu.CurrentCommutatorId = Id;
            bdpu.Cost = 0;
            
            SendBdpu(bdpu);
        }


        private void Print()
        {
            Console.WriteLine("Commutator #{0} - My lord is {1}", Id, ElectionPackage.RootCommutatorId);
        }

        public void Election()
        {
            CreateElectionPackage();
            int count = 5;
            while (count-- > 0)
            {
                Thread.Sleep(SleepTimeForElection);
                foreach (var port in Ports)
                {
                    CheckBdpu(port.Neighbour);
                }
            }
            IsRoot = (ElectionPackage.RootCommutatorId == Id);
            rootCommutator = ElectionPackage.RootCommutatorId;
        }

        private void CheckBdpu(Commutator neighbour)
        {
            lock (Environment.LockObj)
            {
                if (neighbour.Priority > ElectionPackage.Priority)
                {
                    neighbour.ElectionPackage = ElectionPackage;
                }
            }
        }


        private void CreateElectionPackage()
        {
            ElectionPackage = new ElectionPackage();
            ElectionPackage.RootCommutatorId = Id;
            ElectionPackage.Priority = Priority;
        }
    }
}
