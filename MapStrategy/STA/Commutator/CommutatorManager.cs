﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using STA.Network;

namespace STA.Commutator
{
    public class CommutatorManager
    {
        private readonly List<Commutator> _commutators;

        public CommutatorManager(List<Commutator> commutators)
        {
            _commutators = commutators;
            Start();
        }

        public void Start()
        {
            foreach (var commutator in _commutators)
            {
                var thread = new Thread(commutator.Life);
                thread.Start();
            }
        }
    }
}
