﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using STA.Commutator;

namespace STA
{
    class Program
    {
        static void Main(string[] args)
        {
            var lexer = new Lexer();
            var commutators = lexer.CreateNetwork();
//            var commutators = lexer.GetCommutatorsData("net.xml");
//            var segments = lexer.GetSegmentsData("segments.xml");
            var commutatorManager = new CommutatorManager(commutators);
        }
    }
}
