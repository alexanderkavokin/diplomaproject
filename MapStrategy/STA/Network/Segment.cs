﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using STA.Commutator;

namespace STA.Network
{
    public class Segment
    {
        public List<SegmentNode> Nodes;

        public Segment()
        {
            Nodes = new List<SegmentNode>();
        }
    }
}
