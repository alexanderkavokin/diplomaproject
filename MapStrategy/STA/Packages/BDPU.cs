﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using STA.Commutator;

namespace STA.Packages
{
    public class BDPU
    {
        public int RootCommutatorId { get; set; }
        public int CurrentCommutatorId { get; set; }
        public int Cost { get; set; }
        public List<Commutator.Commutator> WayList = new List<Commutator.Commutator>();

    }
}
