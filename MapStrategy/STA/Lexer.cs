﻿using System;
using System.Collections.Generic;
using System.Xml;
using STA.Commutator;
using STA.Network;

namespace STA
{
    public class Lexer
    {
        private List<Commutator.Commutator> _commutators; 
        public List<Commutator.Commutator> GetCommutatorsData(string path)
        {
            var commutators = new List<Commutator.Commutator>();
            var xDoc = new XmlDocument();
            xDoc.Load(path);
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            // обход всех узлов в корневом элементе
            if (xRoot == null)
            {
                Console.WriteLine("Data file is incorrect");
                return null;
            }
            foreach (XmlNode xnode in xRoot)
            {
                var commutator = new Commutator.Commutator();
                XmlNode idAttr = xnode.Attributes.GetNamedItem("id");
                commutator.Id = int.Parse(idAttr.Value);

                XmlNode priorityAttr = xnode.Attributes.GetNamedItem("priority");
                commutator.Priority = int.Parse(priorityAttr.Value);

                var ports = new List<Port>();
                // обходим все дочерние узлы элемента user

                foreach (XmlNode childnode in xnode.ChildNodes)
                {
                    foreach (XmlNode child in childnode)
                    {
                        var port = new Port();
                        XmlNode portIdAttr = child.Attributes.GetNamedItem("id");
                        port.Id = int.Parse(portIdAttr.Value);

                        XmlNode portPriceAttr = child.Attributes.GetNamedItem("price");
                        port.Price = int.Parse(portPriceAttr.Value);
                        ports.Add(port);
                    }
                }
                commutator.Ports = ports;
                commutators.Add(commutator);
            }
            _commutators = commutators;
            return commutators;
        }

        public List<Commutator.Commutator> GetNetwork(string path)
        {
            var result = new List<Commutator.Commutator>();
            var xDoc = new XmlDocument();
            xDoc.Load(path);
            XmlElement xRoot = xDoc.DocumentElement;
            if (xRoot == null)
            {
                Console.WriteLine("Data file is incorrect");
                return null;
            }
            foreach (XmlNode commutatorNode in xRoot)
            {
                var commutator = new Commutator.Commutator();
                XmlNode idAttr = commutatorNode.Attributes.GetNamedItem("id");
                commutator.Id = int.Parse(idAttr.Value);
                XmlNode priorityAttr = commutatorNode.Attributes.GetNamedItem("priority");
                commutator.Priority = int.Parse(priorityAttr.Value);

                foreach (XmlNode portNode in commutatorNode)
                {
                    var port = new Port();
                    idAttr = portNode.Attributes.GetNamedItem("id");
                    var priceAttr = portNode.Attributes.GetNamedItem("price");
                    var neighbourCommutatorId = portNode.Attributes.GetNamedItem("neighbourCommutator");
                    var neighbourPortId = portNode.Attributes.GetNamedItem("neighbourPort");

                    port.Id = int.Parse(idAttr.Value);
                    port.Price = int.Parse(priceAttr.Value);
                    port.NeighbourId = int.Parse(neighbourCommutatorId.Value);
                    port.NeighbourPortId = int.Parse(neighbourPortId.Value);
                    port.Owner = commutator;
                    commutator.Ports.Add(port);
                }
                result.Add(commutator);
            }
            return result;
        }

        public List<Commutator.Commutator> CreateNetwork()
        {
            var network = GetNetwork("network.xml");
            FillPorts(ref network);
            return network;
        }

        private void FillPorts(ref List<Commutator.Commutator> commutators)
        {
            foreach (var commutator in commutators)
            {
                foreach (var port in commutator.Ports)
                {
                    port.Neighbour = commutators[port.NeighbourId - 1];
                    port.NeighbourPort = port.Neighbour.Ports[port.NeighbourPortId - 1];
                }
            }
        }

        public List<Segment> GetSegmentsData(string path)
        {
            var segments = new List<Segment>();
            var xDoc = new XmlDocument();
            xDoc.Load(path);
            XmlElement xRoot = xDoc.DocumentElement;
            if (xRoot == null)
            {
                Console.WriteLine("Data file is incorrect");
                return null;
            }
            foreach (XmlNode xnode in xRoot)
            {
                var segment = new Segment();
                foreach (XmlNode node in xnode.ChildNodes)
                {
                    var segmentNode = new SegmentNode();
                    XmlNode commutatorIdAttr = node.Attributes.GetNamedItem("commutatorId");
                    segmentNode.Commutator = _commutators[int.Parse(commutatorIdAttr.Value)-1];
                    XmlNode portIdAttr = node.Attributes.GetNamedItem("portId");
                    segmentNode.Port = segmentNode.Commutator.Ports[int.Parse(portIdAttr.Value)-1];
                    segment.Nodes.Add(segmentNode);
                }
                segments.Add(segment);
            }
            return segments;
        }
    }
}
