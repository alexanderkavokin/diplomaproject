﻿namespace MultiagentSystem.Environment.GameObjects
{
    public class UnknownPixel : GameObject
    {
        public UnknownPixel()
        {
            Symbol = '?';
        }
    }
}
