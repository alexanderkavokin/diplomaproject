﻿using System.Collections.Generic;

namespace MultiagentSystem.Environment.GameObjects
{
    public class FreeSpace : GameObject
    {
        public FreeSpace()
        {
            Symbol = '.';
        }

        public bool IsDangerous { get; set; }
    }
}
