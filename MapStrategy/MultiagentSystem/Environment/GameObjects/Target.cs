﻿namespace MultiagentSystem.Environment.GameObjects
{
    public class Target : GameObject
    {
        public Target()
        {
            Symbol = '$';
        }
        public int DangerousRadius { get; set; }
    }
}
