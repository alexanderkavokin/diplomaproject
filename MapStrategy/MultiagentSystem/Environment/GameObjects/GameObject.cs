﻿namespace MultiagentSystem.Environment.GameObjects
{
    public abstract class GameObject
    {
        public char Symbol { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int PriceG { get; set; }
        public int PriceF { get; set; }
        public GameObject Parent { get; set; }
        public int LastSeen = 0;
    }
}
