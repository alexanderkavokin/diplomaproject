﻿namespace MultiagentSystem.Environment.GameObjects
{
    
    public class Obstacle : GameObject
    {
        public Obstacle()
        {
            Symbol = '#';
        }
    }
}
