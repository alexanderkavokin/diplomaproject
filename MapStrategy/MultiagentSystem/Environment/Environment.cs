﻿using System.Collections.Generic;
using System.Threading;

namespace MultiagentSystem.Environment
{
    public class Environment
    {
        public MapManager MapManager { get; set; }
        public int Size { get; set; }
        private const int Delay = 200;
        public List<Agent.Agent> Agents = new List<Agent.Agent>();
        

        public void UpdateMap()
        {
            while (true)
            {
                MapManager.PrintMap();
                Thread.Sleep(Delay);
            }
        }
    }
}