﻿using System;
using MultiagentSystem.Environment.GameObjects;
using MultiagentSystem.Printer;

namespace MultiagentSystem.Environment
{
    public class MapManager
    {
        private readonly int _width;
        private readonly int _height;
        private const double Border = 0.95;
        public GameObject[,] Map;
        public MapManager(int width, int  height)
        {
            _width = width;
            _height = height;
            Map = new GameObject[_height, _width];
        }

        public void CretaeMap(bool isTest = false)
        {
            var random = new Random();
            for (int i = 0; i < _height; i++)
            {
                for (int j = 0; j < _width; j++)
                {
                    if (isTest)
                    {
                        if (i == 3 && j != 0 && j != _width - 1)
                        {
                            Map[i, j] = new Obstacle { X = i, Y = j };
                        }
                        else
                        {
                            Map[i, j] = new FreeSpace { X = i, Y = j };
                        }
                    }
                    else
                    {
                        if (random.NextDouble() > Border)
                        {
                            Map[i, j] = new Obstacle { X = i, Y = j };
                        }
                        else
                        {
                            Map[i, j] = new FreeSpace { X = i, Y = j };
                        }
                    }
                }
            }
        }

        public void PrintMap()
        {
            var printer = new ConsolePrinter();
            for (int i = 0; i < _height; i++)
            {
                for (int j = 0; j < _width; j++)
                {
                    printer.PrintSymbol(Map[i, j].Symbol, j == _width - 1);
                }
            }
        }
    }
}
