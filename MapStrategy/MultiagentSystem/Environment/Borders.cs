﻿namespace MultiagentSystem.Environment
{
    public struct Borders
    {
        public int MinX;
        public int MaxX;
        public int MinY; 
        public int MaxY;
    }
}
