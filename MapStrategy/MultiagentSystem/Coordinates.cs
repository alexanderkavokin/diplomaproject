﻿namespace MultiagentSystem
{
    public struct Coordinates
    {
        public int X;
        public int Y;
    }
}
