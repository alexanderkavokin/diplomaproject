﻿using System;
using System.Threading;
using MultiagentSystem.Environment;
using MultiagentSystem.Environment.GameObjects;

namespace MultiagentSystem.Agent
{
    public class AgentManager
    {
        private readonly Environment.Environment _environment;
        private Random _rand;
        public AgentManager(Environment.Environment environment)
        {
            _environment = environment;
        }
        private void CreateAgent(int number)
        {
            var agent = new Agent();
            agent.Id = number;
            agent.ViewRadius = 5;
            agent.RadioRadius = 7;
            agent.Borders = SetBorders(number, _environment.Size);
            var coordinates = SetCoordinates(agent.Borders);
            agent.X = coordinates.X;
            agent.Y = coordinates.Y;
            _environment.MapManager.Map[agent.X, agent.Y] = agent;
            agent.KnownMap = SetKnownMap();
            agent.Init(_environment);
            _environment.Agents.Add(agent);
            var thread = new Thread(agent.Life);
            thread.Start();
        }

        private GameObject[,] SetKnownMap()
        {
            var size = _environment.Size;
            var map = new GameObject[size, size];
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    map[j,i] = new UnknownPixel
                    {
                        X = j,
                        Y = i
                    };
                }
            }
            return map;
        }

        private Borders SetBorders(int number, int size)
        {
            var borders = new Borders();
//            borders.MinX = 0;
//            borders.MinY = 0;
//            borders.MaxX = size;
//            borders.MaxY = size;

            borders.MinX = size / 3 * (number % 3);
            borders.MaxX = size / 3 * (number % 3 + 1);
            borders.MinY = size / 3 * (number / 3);
            borders.MaxY = size / 3 * (number / 3 + 1);
            return borders;
        }

        private Coordinates SetCoordinates(Borders borders)
        {
            var coordinates = new Coordinates();
            while (true)
            {
                var x = _rand.Next(borders.MinX, borders.MaxX);
                var y = _rand.Next(borders.MinY, borders.MaxY);
                if (!(_environment.MapManager.Map[y, x] is FreeSpace)) continue;
                coordinates.X = x;
                coordinates.Y = y;
                break;
            }
            return coordinates;
        }

        public void CreateAgents(int count)
        {
            _rand = new Random();
            for (int i = 0; i < count; i++)
            {
                CreateAgent(i);
            }
        }
    }
}
