﻿using MultiagentSystem.Environment.GameObjects;

namespace MultiagentSystem.Agent.Vision
{
    public class VisionManager : Manager
    {
        private readonly Environment.Environment _environment;
        private readonly Agent _agent;
        public VisionManager(Environment.Environment environment, Agent agent)
        {
            _environment = environment;
            _agent = agent;
        }

        public void GetVisibleInformation()
        {
            for (int i = 0; i < _environment.Size; i++)
            {
                for (int j = 0; j < _environment.Size; j++)
                {
                    var gameObject = _environment.MapManager.Map[i, j];
                    gameObject.LastSeen++;
                    if (IsObjectReachable(gameObject, _agent, _agent.ViewRadius))
                    {
                        lock (_agent.LockObject)
                        {
                            if (gameObject is Agent) {
                                _agent.KnownMap[i, j] = new FreeSpace
                                {
                                    X = i,
                                    Y = j,
                                    LastSeen = 0
                                };
                            }
                            else
                            {
                                _agent.KnownMap[i, j] = gameObject;
                            }
                            gameObject.LastSeen = 0;
                        }
                    }
                }
            }
        }

    }
}
