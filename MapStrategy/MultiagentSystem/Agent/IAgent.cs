﻿namespace MultiagentSystem.Agent
{
    interface IAgent
    {
        int Id { get; set; }
        int ViewRadius { get; set; }
        int RadioRadius { get; set; }

    }
}
