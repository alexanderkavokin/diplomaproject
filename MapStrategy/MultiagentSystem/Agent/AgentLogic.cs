﻿using System.Threading;
using MultiagentSystem.Agent.AI.Movement;
using MultiagentSystem.Agent.Radio;
using MultiagentSystem.Agent.Vision;
using MultiagentSystem.Printer;

namespace MultiagentSystem.Agent
{
    public partial class Agent
    {
        public void Init(Environment.Environment environment)
        {
            _environment = environment;
            _visionManager = new VisionManager(_environment, this);
            _movementManager = new MovementManager(this, _environment);
            _radioManager = new RadioManager(_environment, this);
        }

        public void Life()
        {
            var printer = new FilePrinter();
            while (true)
            {
                _visionManager.GetVisibleInformation();
                _radioManager.SendMapMessages();
                printer.PrintMap("Agent" + Id, KnownMap, _environment.Size);
                _movementManager.SetOptimalDirection();
                Thread.Sleep(Delay);
            }
        }
    }
}
