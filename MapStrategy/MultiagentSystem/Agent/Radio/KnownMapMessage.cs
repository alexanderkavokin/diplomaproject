﻿using MultiagentSystem.Environment.GameObjects;

namespace MultiagentSystem.Agent.Radio
{
    public class KnownMapMessage
    {
        public Agent SenderAgent { get; set; }
        public Agent RecepientAgent { get; set; }
        public GameObject[,] Data { get; set; }

        public void Send()
        {
            lock (RecepientAgent.LockObject)
            {
                foreach (var gameObject in Data)
                {
                    if (gameObject is UnknownPixel) continue;
                    if (!(RecepientAgent.KnownMap[gameObject.X, gameObject.Y] is UnknownPixel))
                    {
                        if (gameObject.LastSeen > RecepientAgent.KnownMap[gameObject.X, gameObject.Y].LastSeen) continue;
                    }
                    RecepientAgent.KnownMap[gameObject.X, gameObject.Y] = gameObject;
                }
            }
        }
    }
}
