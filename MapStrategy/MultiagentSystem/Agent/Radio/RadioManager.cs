﻿using System;
using System.Collections.Generic;
using MultiagentSystem.Environment;

namespace MultiagentSystem.Agent.Radio
{
    public class RadioManager : Manager
    {
        private readonly Environment.Environment _environment;
        private readonly Agent _agent;
        public RadioManager(Environment.Environment environment, Agent agent)
        {
            _environment = environment;
            _agent = agent;
        }

        

        private IEnumerable<Agent> FindRadioNeighbours()
        {
            var borders = GetSearchArea(_agent, _agent.RadioRadius, _environment.Size);
            var agents = new List<Agent>();
            for (int i = borders.MinX; i < borders.MaxX; i++)
            {
                for (int j = borders.MinY; j < borders.MaxY; j++)
                {
                    var gameObject = _environment.MapManager.Map[i, j];
                    if (gameObject is Agent)
                    {
                        if (IsObjectReachable(gameObject, _agent, _agent.RadioRadius))
                        {
                            agents.Add(gameObject as Agent);
                        }
                    }
                }
            }
            return agents;
        }

        public void SendMapMessages()
        {
            var neighbours = FindRadioNeighbours();
            foreach (var neighbour in neighbours)
            {
                var message = new KnownMapMessage();
                message.SenderAgent = _agent;
                message.RecepientAgent = neighbour;
                message.Data = _agent.KnownMap;
                message.Send();
            }
        }
    }
}
