﻿using MultiagentSystem.Agent.AI.Movement;
using MultiagentSystem.Agent.Radio;
using MultiagentSystem.Agent.Vision;
using MultiagentSystem.Environment;
using MultiagentSystem.Environment.GameObjects;

namespace MultiagentSystem.Agent
{
    public partial class Agent : GameObject
    {
        public Agent()
        {
            Symbol = 'o';
        }
        public int Id { get; set; }
        public Borders Borders { get; set; }
        public int ViewRadius { get; set; }
        public int RadioRadius { get; set; }
        public GameObject[,] KnownMap { get; set; }

        public object LockObject = new object();

        private VisionManager _visionManager;
        private MovementManager _movementManager;
        private RadioManager _radioManager;
        private Environment.Environment _environment;
        private const int Delay = 200;
    }
}
