﻿using MultiagentSystem.Environment.GameObjects;

namespace MultiagentSystem.Agent.AI.Movement
{
    public class Direction
    {
        public DirectionType DirectionType { get; set; }
        public int Rating { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public bool IsFree { get; set; }
        public GameObject GameObject { get; set; }
        public GameObject MostRemoteGameObject { get; set; }
    }
}
