﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MultiagentSystem.Environment;
using MultiagentSystem.Environment.GameObjects;

namespace MultiagentSystem.Agent.AI.Movement
{
    public class MovementManager : Manager
    {
        private readonly Agent _agent;
        private readonly List<Direction> _directions;
        private readonly Environment.Environment _environment;
        private Random _random;
        private FreeSpace _leftRemote, _rightRemote, _upRemote, _downRemote;
        public MovementManager(Agent agent, Environment.Environment environment)
        {
            _agent = agent;
            _environment = environment;
            _directions = new List<Direction>();
            CreateDirections();
            _random = new Random();
        }

        private void CreateDirections()
        {
            _directions.Add(new Direction { DirectionType = DirectionType.Up, Rating = 0 });
            _directions.Add(new Direction { DirectionType = DirectionType.Down, Rating = 0 });
            _directions.Add(new Direction { DirectionType = DirectionType.Left, Rating = 0 });
            _directions.Add(new Direction { DirectionType = DirectionType.Right, Rating = 0 });
        }

        private void CalculateRating(Borders borders)
        {
            for (int i = borders.MinY; i < borders.MaxY; i++)
            {
                for (int j = borders.MinX; j < borders.MaxX; j++)
                {
                    var gameObject = _agent.KnownMap[j, i];
                    if (gameObject is UnknownPixel)
                    {
                        if (gameObject.Y < _agent.Y)
                        {
                            _directions.Find(direction => direction.DirectionType == DirectionType.Up).Rating++;
                        }
                        if (gameObject.Y > _agent.Y)
                        {
                            _directions.Find(direction => direction.DirectionType == DirectionType.Down).Rating++;
                        }
                        if (gameObject.X < _agent.X)
                        {
                            _directions.Find(direction => direction.DirectionType == DirectionType.Left).Rating++;
                        }
                        if (gameObject.X > _agent.X)
                        {
                            _directions.Find(direction => direction.DirectionType == DirectionType.Right).Rating++;
                        }
                    }
                }
            }
        }

        private void ClearRatings()
        {
            foreach (var direction in _directions)
            {
                direction.Rating = 0;
                direction.IsFree = false;
                direction.GameObject = null;
            }
            GetMostRemotePoints();
            var currentDirection = _directions.Find(direction => direction.DirectionType == DirectionType.Left);
            currentDirection.X = _agent.X - 1;
            currentDirection.Y = _agent.Y;
            currentDirection.MostRemoteGameObject = _leftRemote;

            currentDirection = _directions.Find(direction => direction.DirectionType == DirectionType.Right);
            currentDirection.X = _agent.X + 1;
            currentDirection.Y = _agent.Y;
            currentDirection.MostRemoteGameObject = _rightRemote;

            currentDirection = _directions.Find(direction => direction.DirectionType == DirectionType.Up);
            currentDirection.X = _agent.X;
            currentDirection.Y = _agent.Y - 1;
            currentDirection.MostRemoteGameObject = _upRemote;

            currentDirection = _directions.Find(direction => direction.DirectionType == DirectionType.Down);
            currentDirection.X = _agent.X;
            currentDirection.Y = _agent.Y + 1;
            currentDirection.MostRemoteGameObject = _downRemote;
        }

        public void SetOptimalDirection()
        {
            ClearRatings();
            CalculateRating(_agent.Borders);
            IsPossibleMovement();
            var safeDirections = CheckForDanger();
//            var freeDirections = safeDirections.Where(direction => direction.IsFree).ToList();
            var sortedDirections = safeDirections.OrderByDescending(direction => direction.Rating).ToList();
            if (sortedDirections.Count == 0) return;
            var selectedDirection = sortedDirections[0];
//            if (_random.NextDouble() > 0.7)
//            {
//                selectedDirection = sortedDirections[_random.Next(sortedDirections.Count - 1)];
//            }
//            Move(selectedDirection);

            MoveToMostRemoteSpace(selectedDirection.MostRemoteGameObject);
        }

        public List<Direction> CheckForDanger()
        {
            var safeDirections = new List<Direction>();
            foreach (var direction in _directions)
            {
                if (direction.GameObject is FreeSpace)
                {
                    if (!(direction.GameObject as FreeSpace).IsDangerous)
                    {
                        safeDirections.Add(direction);
                    }
                }
            }
            return safeDirections;
        }

        private void MoveToMostRemoteSpace(GameObject remoteSpace)
        {
            var astar = new AStar(_agent, remoteSpace, _environment.MapManager.Map, _environment.Size, _agent.Borders);
            if (remoteSpace.X == 9 && remoteSpace.Y == 1)
            {

            }
            astar.Start();
            var way = astar.Way;
            foreach (var freeSpace in way)
            {
                if (freeSpace.X == 9 && freeSpace.Y == 9)
                {

                }
                MoveToNextPosition(freeSpace);
            }
        }

        private void MoveToNextPosition(FreeSpace newPosition)
        {
            if (newPosition.X == 9 && newPosition.Y == 9)
            {
                
            }
            _agent.KnownMap[_agent.X, _agent.Y] = new FreeSpace
            {
                LastSeen = 0,
                X = _agent.X,
                Y = _agent.Y
            };
            _environment.MapManager.Map[_agent.X, _agent.Y] = new FreeSpace
            {
                X = newPosition.X,
                Y = newPosition.Y
            };
            _environment.MapManager.Map[newPosition.X, newPosition.Y] = _agent;

            _agent.X = newPosition.X;
            _agent.Y = newPosition.Y;
            Thread.Sleep(300);
        }

        private void Move(Direction direction)
        {
            _agent.KnownMap[_agent.X, _agent.Y] = new FreeSpace
            {
                LastSeen = 0,
                X = _agent.X,
                Y = _agent.Y
            };
            _environment.MapManager.Map[_agent.X, _agent.Y] = new FreeSpace
            {
                X = direction.X,
                Y = direction.Y
            };
            _environment.MapManager.Map[direction.X, direction.Y] = _agent;
                
            _agent.X = direction.X;
            _agent.Y = direction.Y;
        }

        private void IsPossibleMovement()
        {
            foreach (var direction in _directions)
            {
                if (IsOutOfBorders(_agent.Borders, direction.X, direction.Y))
                {
                    direction.IsFree = false;
                    direction.GameObject = null;
                    continue;
                }
                direction.IsFree = _agent.KnownMap[direction.X, direction.Y] is FreeSpace;
                direction.GameObject = _environment.MapManager.Map[direction.X, direction.Y];
            }
        }


        private bool IsOutOfBorders(Borders borders, int x, int y)
        {
            return (x < borders.MinX || x >= borders.MaxX) || (y < borders.MinY || y >= borders.MaxY);
        }
        
        private void GetMostRemotePoints()
        {
            for (int i = _agent.Borders.MinX; i < _agent.Borders.MaxX; i++)
            {
                for (int j = _agent.Borders.MinY; j < _agent.Borders.MaxY; j++)
                {
                    var point = _agent.KnownMap[i, j];
                    if (!(point is FreeSpace)) continue;
                    if (_leftRemote == null)
                    {
                        _leftRemote = point as FreeSpace;
                        _rightRemote = point as FreeSpace;
                        _upRemote = point as FreeSpace;
                        _downRemote = point as FreeSpace;
                    }
                    if (point.X < _leftRemote.X) _leftRemote = point as FreeSpace;
                    if (point.X > _leftRemote.X) _rightRemote = point as FreeSpace;
                    if (point.Y < _leftRemote.Y) _upRemote = point as FreeSpace;
                    if (point.Y > _leftRemote.Y) _downRemote = point as FreeSpace;
                }
            }
        }
    }
}
