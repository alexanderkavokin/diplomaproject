﻿namespace MultiagentSystem.Agent.AI.Movement
{
    public enum DirectionType
    {
        Up,
        Down,
        Left,
        Right
    }
}
