﻿using System;
using System.Collections.Generic;
using MultiagentSystem.Environment;
using MultiagentSystem.Environment.GameObjects;

namespace MultiagentSystem.Agent.AI
{
    public class AStar
    {
        private readonly List<GameObject> _openedList = new List<GameObject>();
        private readonly List<GameObject> _closedList = new List<GameObject>();
        private readonly GameObject _start;
        private readonly GameObject _finish;
        private readonly GameObject[,] _map;
        public readonly List<FreeSpace> Way = new List<FreeSpace>(); 
        private readonly int _size;
        private Borders _borders;

        public AStar(GameObject start, GameObject finish, GameObject[,] map, int size, Borders borders)
        {
            _start = start;
            _finish = finish;
            _map = map;
            _size = size;
            _borders = borders;
        }

        public void Start()
        {
            _openedList.Add(_start);
            var neighbours = GetNeighbours(_start);
            foreach (var gameObject in neighbours)
            {
                gameObject.Parent =_start;
            }
            _openedList.AddRange(neighbours);

            _openedList.Remove(_start);
            _closedList.Add(_start);
            while (true)
            {
                if (_openedList.Count == 0) break;
                var best = GetPointWithMinPrice(_openedList);
                
                Algorithm(best);
                if (best == _finish) break;
            }
            GetWay(_finish as FreeSpace);
            Way.Reverse();
        }
        private void GetWay(FreeSpace point)
        {
            if (point == _start) return;
            Way.Add(point);
            if (!(point.Parent is FreeSpace))
            {
                return;
            }
//            point.Symbol = '1';
            GetWay(point.Parent as FreeSpace);
        }

        private void Algorithm(GameObject point)
        {
            if (point == _finish)
            {
                
            }
            _openedList.Remove(point);
            _closedList.Add(point);
            var neighbours = GetNeighbours(point);
            foreach (var gameObject in neighbours)
            {
                gameObject.Parent = point;
            }
            foreach (var neighbour in neighbours)
            {
                if (!_openedList.Contains(neighbour))
                {
                    _openedList.Add(neighbour);
                }
                else
                {
                    if (neighbour.PriceG < point.PriceG + 10) continue;
                    neighbour.Parent = point;
                }
            }

        }

        private GameObject GetPointWithMinPrice(List<GameObject> points)
        {
            var min = points[0];
            foreach (var freeSpace in points)
            {
                freeSpace.PriceF = CalculateF(freeSpace);
                if (min.PriceF == 0 || freeSpace.PriceF < min.PriceF) min = freeSpace;
            }
            return min;
        }

        private List<FreeSpace> GetNeighbours(GameObject point)
        {
            var list = new List<FreeSpace>();
            GameObject neighbour;
            if (point.X - 1 >= _borders.MinX)
            {
                neighbour = _map[point.X - 1, point.Y];
                if (neighbour is FreeSpace && !_closedList.Contains(neighbour) && !(neighbour as FreeSpace).IsDangerous) list.Add(neighbour as FreeSpace);
            }
            if (point.X + 1 < _borders.MaxX)
            {
                neighbour = _map[point.X + 1, point.Y];
                if (neighbour is FreeSpace && !_closedList.Contains(neighbour) && !(neighbour as FreeSpace).IsDangerous) list.Add(neighbour as FreeSpace);
            }
            if (point.Y - 1 >= _borders.MinY)
            {
                neighbour = _map[point.X, point.Y - 1];
                if (neighbour is FreeSpace && !_closedList.Contains(neighbour) && !(neighbour as FreeSpace).IsDangerous) list.Add(neighbour as FreeSpace);
            }
            if (point.Y + 1 < _borders.MaxY)
            {
                neighbour = _map[point.X, point.Y + 1];
                if (neighbour is FreeSpace && !_closedList.Contains(neighbour)) list.Add(neighbour as FreeSpace);
            }
            
            return list;
        }

        private int CalculateF(GameObject point)
        {
            CalculateG(point);
            var h = CalculateH(point);
            return h + point.PriceG;
        }

        private void CalculateG(GameObject point)
        {
            point.PriceG = (point.Parent).PriceG + 10;
        }

        private int CalculateH(GameObject point)
        {
            int x = Math.Abs(point.X - _finish.X);
            int y = Math.Abs(point.Y - _finish.Y);
            int h = (x + y)*10;
            return h;
        }
    }
}
