﻿using System;
using MultiagentSystem.Environment;
using MultiagentSystem.Environment.GameObjects;

namespace MultiagentSystem.Agent
{
    public abstract class Manager
    {
        public bool IsObjectReachable(GameObject gameObject, GameObject agent, int radius)
        {
            var distance = Math.Sqrt(Math.Pow(gameObject.X - agent.X, 2) + Math.Pow(gameObject.Y - agent.Y, 2));
            return distance <= radius;
        }

        public Borders GetSearchArea(GameObject gameObject, int radius, int size)
        {
            var borders = new Borders();
            borders.MinX = Math.Max(0, gameObject.X - radius);
            borders.MinY = Math.Max(0, gameObject.Y - radius);
            borders.MaxX = Math.Min(size, gameObject.X + radius);
            borders.MaxY = Math.Min(size, gameObject.Y + radius);
            return borders;
        }
    }
}
