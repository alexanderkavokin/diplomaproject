﻿using System;
using System.Linq;
using MultiagentSystem.Agent;
using MultiagentSystem.Environment.GameObjects;

namespace MultiagentSystem.Target
{
    public class TargetManager : Manager
    {
        private readonly Environment.Environment _environment;
        public TargetManager(Environment.Environment environment)
        {
            _environment = environment;
        }

        public void CreateTarget()
        {
            var random = new Random();
            bool isSafePosition = false;
            var target = new Environment.GameObjects.Target();
            while (!isSafePosition)
            {
                target = new Environment.GameObjects.Target
                {
                    DangerousRadius = 3,
                    X = random.Next(_environment.Size),
                    Y = random.Next(_environment.Size)
                };
                isSafePosition = _environment.Agents.All(agent => !IsObjectReachable(target, agent, target.DangerousRadius));
            }
            _environment.MapManager.Map[target.X, target.Y] = target;
            CreateDangerousArea(target);
        }

        private void CreateDangerousArea(Environment.GameObjects.Target target)
        {
            var borders = GetSearchArea(target, target.DangerousRadius, _environment.Size);
            for (int i = borders.MinX; i < borders.MaxX; i++)
            {
                for (int j = borders.MinY; j < borders.MaxY; j++)
                {
                    var gameObject = _environment.MapManager.Map[i, j];
                    if (gameObject is FreeSpace)
                    {
                        if (IsObjectReachable(gameObject, target, target.DangerousRadius))
                        {
                            (gameObject as FreeSpace).IsDangerous = true;
                        }
                    }
                }
            }
        }
    }
}
