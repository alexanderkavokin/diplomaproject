﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiagentSystem.Printer
{
    public interface IPrinter
    {
        void PrintSymbol(char symbol, bool isEoS);
    }
}
