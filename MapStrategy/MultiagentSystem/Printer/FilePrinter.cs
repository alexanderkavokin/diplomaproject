﻿using System.IO;
using MultiagentSystem.Environment.GameObjects;

namespace MultiagentSystem.Printer
{
    public class FilePrinter : IPrinter
    {
        public void PrintSymbol(char symbol, bool isEoS)
        {
            
        }

        public void PrintMap(string path, GameObject[,] map, int size)
        {
            using (var writer = new StreamWriter(path+".txt"))
            {
                for (int i = 0; i < size; i++)
                {
                    for (int j = 0; j < size; j++)
                    {
                        writer.Write(map[i,j].Symbol);
                    }
                    writer.WriteLine();
                }
            }
        }
    }
}
