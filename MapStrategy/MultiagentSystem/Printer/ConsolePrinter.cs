﻿using System;

namespace MultiagentSystem.Printer
{
    public class ConsolePrinter : IPrinter
    {
        public ConsolePrinter()
        {
            Console.Clear();
        }
        public void PrintSymbol(char symbol, bool isEoS)
        {
            if (isEoS)
            {
                Console.WriteLine(symbol);
            }
            else
            {
                Console.Write(symbol);
            }
        }
    }
}
