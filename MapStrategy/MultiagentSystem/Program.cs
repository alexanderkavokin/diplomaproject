﻿using System;
using System.Threading;
using MultiagentSystem.Agent;
using MultiagentSystem.Agent.AI;
using MultiagentSystem.Environment;
using MultiagentSystem.Environment.GameObjects;
using MultiagentSystem.Target;

namespace MultiagentSystem
{
    class Program
    {
        private static MapManager _mapManager;
        private static readonly Environment.Environment Environment = new Environment.Environment();

        static void Main(string[] args)
        {
            CreateMap();
            StartAgentManager();
            StartTargetManager();
            
            PrintMap();
            Console.ReadLine();
        }

        static void CreateMap()
        {
            Console.WriteLine("Enter size of map");
            int width;
            while (!int.TryParse(Console.ReadLine(), out width) || width <= 0)
            {
                Console.WriteLine("Incorrect input. Enter positive number, please");
            }
            _mapManager = new MapManager(width, width);
            _mapManager.CretaeMap();
            Environment.MapManager = _mapManager;
            Environment.Size = width;
        }

        static void PrintMap()
        {
            var thread = new Thread(Environment.UpdateMap);
            thread.Start();
        }

        static void StartAgentManager()
        {
            var agentManager = new AgentManager(Environment);
            agentManager.CreateAgents(9);

        }

        static void StartTargetManager()
        {
            var targetManager = new TargetManager(Environment);
            targetManager.CreateTarget();
        }
    }
}
